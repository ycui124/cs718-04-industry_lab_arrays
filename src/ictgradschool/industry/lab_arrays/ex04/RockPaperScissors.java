package ictgradschool.industry.lab_arrays.ex04;

import ictgradschool.Keyboard;

/**
 * A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;
    public static final int SCISSORS = 2;
    public static final int PAPER = 3;

    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.

    public void start() {

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        // as detailed in the exercise sheet.
        System.out.print("Hi! What is your name? ");
        String playerName = Keyboard.readInput();
        int playerNum = 0;
        String result;
        while (true) {
            System.out.println();
            System.out.println("1. Rock");
            System.out.println("2. Scissors");
            System.out.println("3. Paper");
            System.out.println("4. Quit");
            System.out.print("Enter choice: ");
            playerNum = Integer.parseInt(Keyboard.readInput());
            if (playerNum != 4) {
                System.out.println();
                int comNum = (int) (Math.random() * 2 + 1);
                displayPlayerChoice(playerName, playerNum);
                displayPlayerChoice("Computer", comNum);
                if (userWins(playerNum, comNum)) {
                    System.out.println(playerName + " wins because " + getResultString(playerNum, comNum));
                } else if (playerNum == comNum) {
                    System.out.println("No one wins.");
                } else {
                    System.out.println("The computer wins because " + getResultString(playerNum, comNum));
                }
            } else {
                break;
            }
        }
        System.out.println();
        System.out.println("Goodbye " + playerName + ". Thanks for playing :)");
    }


    public void displayPlayerChoice(String name, int choice) {
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)
        String chose;
        if (choice == 1) {
            chose = "ROCK";
        } else if (choice == 2) {
            chose = "SCISSORS";
        } else {
            chose = "PAPER";
        }
        System.out.println(name + " chose " + chose + ".");
    }

    public boolean userWins(int playerChoice, int computerChoice) {
        // TODO Determine who wins and return true if the player won, false otherwise.
        if (playerChoice == 1 && computerChoice == 2||playerChoice == 2 && computerChoice == 3
                ||playerChoice==3&&computerChoice == 1) {
            return true;
        } else {
            return false;
        }
    }

    public String getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "paper covers rock.";
        final String ROCK_WINS = "rock smashes scissors.";
        final String SCISSORS_WINS = "scissors cut paper.";
        final String TIE = " you chose the same as the computer.";

        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.
        String result = "";
        if (playerChoice == computerChoice) {
            result = TIE;
        } else if (playerChoice == 1 && computerChoice == 2 || playerChoice == 2 && computerChoice == 1) {
            result = ROCK_WINS;
        } else if (playerChoice == 1 && computerChoice == 3 || playerChoice == 3 && computerChoice == 1) {
            result = PAPER_WINS;
        } else if (playerChoice == 2 && computerChoice == 3 || playerChoice == 3 && computerChoice == 2) {
            result = SCISSORS_WINS;
        }
        return result;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
