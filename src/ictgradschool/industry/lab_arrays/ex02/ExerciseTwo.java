package ictgradschool.industry.lab_arrays.ex02;

public class ExerciseTwo {
    private double getTotal(double[] values) {
        // You complete this for loop
        double totalValue = 0;
        for (int i = 0; i < values.length; i++) {
            totalValue += values[i];
        }
        return totalValue;
    }

    public static void main(String[] args) {
        ExerciseTwo e = new ExerciseTwo();
        double dd=e.getTotal(new double[1]);
        System.out.println(dd);
    }
}
