package ictgradschool.industry.lab_arrays.ex05;

public class Pattern {
    int numOfSymbol;
    char symbol;

    public Pattern(int numOfSymbol, char symbol) {
        this.numOfSymbol = numOfSymbol;
        this.symbol = symbol;
    }

    public int getNumberOfCharacters() {
        return numOfSymbol;
    }

    public void setNumberOfCharacters(int numOfSymbol) {
        this.numOfSymbol = numOfSymbol;
    }

    public char getSymbol() {
        return symbol;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        String words="";
        for(int i=0;i<numOfSymbol;i++){
            words+=symbol;
        }
        return words;
    }
}
