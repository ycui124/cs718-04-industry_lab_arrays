package ictgradschool.industry.lab_arrays.ex01;

public class ExerciseOne {


    public static void main(String[] args) {
        int[] numbers = {-9, 2, 7, 5, 124, -5, 1, 144};
//1. What would be the output of the following statements?
//    a.-9
        System.out.println(numbers[0]);
//    b.144
        System.out.println(numbers[numbers.length - 1]);
//    c.7
        System.out.println(numbers[numbers[1]]);
//    d.-18
        System.out.println(numbers[0] * numbers[1]);
//    e.8
        System.out.println(numbers.length);
//    2. Declare an array of doubles named amounts.
//    3. Construct the amounts array declared in 2) above, big enough to hold 100 elements.
        double[] amounts=new double[100];
//    4. Write a Java statement which assigns 22.75 to element 0 of the amounts array.
        amounts[0]=22.75;
    }


}
